#John Wichita
#12-5-2021
#Bellevue University
#Python programming with Prof. Mack
#final class project weather app



#This project uses the Requests library, and we import that here

import requests, json

#defining the main function
def main():

    api_key = "2c11e070137f56fdc06591ea13a22d34"

    base_url = "http://api.openweathermap.org/data/2.5/weather?q="

    city = input("Enter the Name of City or a zip code:  ")

    complete_url = base_url + city + "&appid=" + api_key

    response = requests.get(complete_url)

    x = response.json()

    # print(x)
    if x["cod"] !="404":
        # store the value of "main"
        # key in variable y
        y = x["main"]
    
        # store the value corresponding
        # to the "temp" key of y
        current_temperature = y["temp"]
    
        # store the value corresponding
        # to the "pressure" key of y
        current_pressure = y["pressure"]
    
        # store the value corresponding
        # to the "humidity" key of y
        current_humidity = y["humidity"]
    
        # store the value of "weather"
        # key in variable z
        z = x["weather"]
    
        # store the value corresponding
        # to the "description" key at
        # the 0th index of z
        weather_description = z[0]["description"]
    
        # print following values
        print(" Temperature (in kelvin unit) = " +
                        str(current_temperature) +
            "\n atmospheric pressure (in hPa unit) = " +
                        str(current_pressure) +
            "\n humidity (in percentage) = " +
                        str(current_humidity) +
            "\n description = " +
                        str(weather_description))
    #error message is provided if they enter invalid information.
    else:
        print("sorry, there must be an error, please check your spelling and try the program again.")

#call the main function and start the program here. Flags within a while loop allow the user to run the program multiple times. 
if __name__ == "__main__":
    flag = False 
    while flag == False:
        main()
        choice = input("\nWould you like to get another weather forcast? y/n: ")
        if choice == "y":
            flag = False
        else:
            flag = True
            print("\nThank you for using this program. Have a nice day! \n")



